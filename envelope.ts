import { Keccak } from 'sha3';
import * as secp256k1 from 'secp256k1';
import { Message, Type, Field } from 'protobufjs/light';

import { Signature, Signer, Serializable } from './interfaces';


let EnvelopeMsg = new Type('EnvelopeMsg');
EnvelopeMsg.add(new Field('content', 1, 'bytes'));
EnvelopeMsg.add(new Field('recid', 2, 'bytes'));
EnvelopeMsg.add(new Field('signature', 3, 'bytes'));
EnvelopeMsg.add(new Field('previousHash', 4, 'bytes'));
EnvelopeMsg.add(new Field('hash', 5, 'bytes'));


// represents signed data.
// can also sign other envelopes
export class Envelope {
	readonly typ:	Uint8Array		// type of object in content buffer
	content:	Uint8Array 		// content buffer
	recid:		Uint8Array 		// recovery id for signature
	signature:	Uint8Array 		// signature for content buffer
	previousHash:	Uint8Array 		// hash of previously signed envelope by signer (linked list)
	hash:		Uint8Array 		// proof to be signed, see digest method for details (this will be the previousHash for next signed envelope)


	constructor(previousHash:Uint8Array, content:Serializable) {
		try {
			Envelope._assertHash(previousHash);
		} catch(e) {
			throw('invalid previousHash of length "' + previousHash +'"');
		}

		this.typ = new Uint8Array([0]);
		this.previousHash = previousHash;
		this.content = content.serialize();
		this.recid = new Uint8Array(0);
		this.signature = new Uint8Array(0);
		this.hash = new Uint8Array(0);
		this._digest();
	}


	// digest is hash(content|previousHash).
	public _digest() {
		let h = new Keccak(256);
		let b = Buffer.from(this.content);
		h.update(b);
		b = Buffer.from(this.previousHash);
		h.update(b);
		this.hash = h.digest();
		return true;
	}


	// sign signs the digest created by Envelope.digest().
	public sign(wallet:Signer): Signature {
		if (this.hash === undefined) {
			throw('digest not ready for hashing');
		}

		let signatureObject = wallet.signDigest(this.hash);
		console.debug('signature successful recid ' + signatureObject.recid + ' sig ' + signatureObject.signature);
		this.signature = signatureObject.signature;
		this.recid = Buffer.from([signatureObject.recid]);
		return {
			signature: signatureObject.signature,
			recid: signatureObject.recid,
		};
	}


	public getSignature(): Signature {
		if (this.signature === undefined) {
			throw('envelope has no signature');
		}
		return {
			signature: this.signature,
			recid: this.recid[0],
		}
	}


	public isSigned(): boolean {
		return this.signature.length != 0;
	}


	// recovers public key from data
	public recover(): Uint8Array {
		if (this.signature === undefined) {
			throw('no signature to recover');
		}
		let hashArray = new Uint8Array(this.hash);
		let signatureArray = new Uint8Array(this.signature);
		let publicKey = secp256k1.ecdsaRecover(signatureArray, this.recid[0], hashArray);
		return publicKey;
	}


	public serialize(): Uint8Array {
		let msg = EnvelopeMsg.fromObject(this);
		let b = EnvelopeMsg.encode(msg).finish();
		return b;
	}


	public static deserialize(serializedEnvelope:Uint8Array): Envelope {
		let b = Buffer.from(serializedEnvelope);
		let msg = EnvelopeMsg.decode(b);
		let o = EnvelopeMsg.toObject(msg);
		console.debug('oooooo ',  o);

		let e = new Envelope(o.previousHash, {
			serialize: () => { return o.content; },
			typ: o.typ,
		});
		e.hash = o.hash;
		e.signature = o.signature;
		e.recid = o.recid;

		return e;
	}


	public toJSON(): any {
		let j = {
			typ: this.typ,
			content: Array.from(this.content),
			recid: Array.from(this.recid),
			signature: Array.from(this.signature),
			previousHash: Array.from(this.previousHash),
			hash: Array.from(this.hash),
		};
		let js = JSON.stringify(j);
		return j;
	}


	//public static fromJson(s:EnvelopeJSON): Envelope {
	public static fromJSON(s:string): Envelope {
		let o = JSON.parse(s);
		let envelope = new Envelope(Uint8Array.from(o.previousHash), {
			serialize: () => { return new Uint8Array(o.content); },
			typ: o.typ,
		});
		let hsh = new Uint8Array(o.hash);
		Envelope._assertHash(hsh);
		envelope.recid = new Uint8Array(o.recid);
		envelope.signature = new Uint8Array(o.signature);
		envelope.hash = new Uint8Array(o.hash);
		return envelope;
	}


	private static _assertHash(h:Uint8Array) {
		if (h.length < 32) {
			throw('previous hash must be 32 bytes');
		}
	}
}
