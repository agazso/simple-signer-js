import { Message, Type, Field } from 'protobufjs/light';
import { Attachment } from './attachment';
import { Contact } from './contact';

let WORK_MAX_WEIGHT = 255;
let WORK_MAX_DESCRIPTION_LENGTH = 255;
let WORK_EARLIEST = 1586272902;

let limits = {
	'weight': 	WORK_MAX_WEIGHT,
	'description': 	WORK_MAX_DESCRIPTION_LENGTH,
	'timestamp':	WORK_EARLIEST,
}

let WorkMsg = new Type('WorkMsg');
WorkMsg.add(new Field('description', 1, 'string'));
WorkMsg.add(new Field('weight', 2, 'uint32'));
WorkMsg.add(new Field('timestamp', 3, 'uint32'));
WorkMsg.add(new Field('attachments', 5, 'bytes'));



class Work {

	readonly typ:		Uint8Array;
	description: 		string;
	weight:			number;
	timestamp:		number;
	attachments:		Attachment[]; // not in use currently but included in case it has consequences for protobuf serialization

	constructor(description:string, weight:number, timestamp:number) {
		try {
			Work._assertDescription(description);
		} catch(e) {
			throw('invalid description value "' + description + '": ' + e);
		}
		try {
			Work._assertWeight(weight);
		} catch(e) {
			throw('invalid weight value "' + weight + '": ' + e);
		}
		try {
			Work._assertTimestamp(timestamp);
		} catch(e) {
			throw('invalid timestamp value "' + timestamp + '": ' + e);
		}

		console.debug('created new work object; description "' + description + '" weight ' + weight);
	
		this.typ = new Uint8Array([1]);
		this.description = description;
		this.weight = weight;
		this.timestamp = timestamp;
		this.attachments = [];
	}



	// NOTE: serialize does not include the contact property
	public serialize(): Uint8Array {
		let msg = WorkMsg.fromObject(this);
		let b = WorkMsg.encode(msg).finish();
		return b;
	}


	// NOTE: deserialize does not include the contact property
	static deserialize(serializedWork:Uint8Array): Work {
		let b = Buffer.from(serializedWork);
		let msg = WorkMsg.decode(b);
		let o = WorkMsg.toObject(msg);

		// // alternately this, but then without input checking
		// let w = Object.create(Work.prototype);
		// Object.assign(w, msg);
		let w = new Work(o.description, o.weight, o.timestamp);

		return w;
	}


	static fromJSON(s:string): Work {
		return JSON.parse(s);
	}


	private static _assertDescription(description:string) {
		if (description.length > WORK_MAX_DESCRIPTION_LENGTH) {
			throw('description too long');
		}
	}


	private static _assertWeight(weight:number) {
		if (weight > 255 || weight < 1) {
			throw('weight value must be in range 1 to ' + WORK_MAX_WEIGHT);
		}
	}


	private static _assertTimestamp(timestamp:number) {
		if (timestamp < WORK_EARLIEST) {
			throw('oh, come on; app wasn\'t even MADE before ' + WORK_EARLIEST);
		}
	}
}

export { limits, Work };
