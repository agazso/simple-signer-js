import * as secp256k1 from 'secp256k1';

import { KeyHolder, Signature } from './interfaces';

let CONTACT_MAX_NAME = 255;

let limits = {
	'name':	CONTACT_MAX_NAME,
}


class Contact {

	commonName:	string		// common name of the contact
	metaUrl:	string		// resource pointers to additional information registered for the contact

	constructor(commonName:string, metaUrl?:string) {
		Contact._assertName(commonName);
		this.commonName = commonName;
		this.metaUrl = '';
		if (metaUrl !== undefined) {
			Contact._assertUrl(metaUrl);
			this.metaUrl = metaUrl;
		}
	}


	private static _assertName(name:string) {
		if (name.length > CONTACT_MAX_NAME) {
			throw('name too long');
		}
	}


	private static _assertUrl(url:string) {
	}
}

class KeyContact extends Contact implements KeyHolder {

	publicKey:	Uint8Array		// public key of contact

	constructor(commonName:string, publicKey:Uint8Array, metaUrl?:string) {
		super(commonName, metaUrl);
		KeyContact._assertPublicKey(publicKey)
		this.publicKey = publicKey;
	}


	// TODO: untested
	public static fromContact(contactBase:Contact, publicKey:Uint8Array): KeyContact {
		let contact = new KeyContact(contactBase.commonName, publicKey, contactBase.metaUrl);
		return contact
	}


	private static _assertPublicKey(key:Uint8Array) {
	}


	public verify(message:Uint8Array, signature:Signature): boolean {
		console.debug('verifying signature of ' + message);
		return secp256k1.ecdsaVerify(signature.signature, message, this.publicKey);
	}

}


export { limits, Contact, KeyContact };
