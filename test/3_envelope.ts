import * as assert from 'assert';

import { Envelope } from '../envelope';
import { Wallet } from '../wallet';
import { zeroHash } from '../types';


let mnemonic = "soup high fat equip protect unusual tag helmet detail erupt hover boss glad crew relief narrow message grid swing tone physical inquiry satisfy habit";
let publicKeyBytes =  new Uint8Array([
	3,
	137,
	224,
	201,
	12,
	151,
	164,
	57,
	124,
	247,
	39,
	234,
	238,
	51,
	12,
	51,
	46,
	84,
	23,
	169,
	41,
	239,
	136,
	115,
	251,
	79,
	155,
	105,
	131,
	232,
	47,
	196,
	76
]);


describe('envelope', () => {
	it('create', function() {

		// zero-hash is a valid value and would irl mean
		// first signature ever for this key
		let previousHash = zeroHash;
		let mockContent = {
			serialize: function() {
				return Uint8Array.from([0x66, 0x6f, 0x6f]);
			},
			typ: new Uint8Array([42]),
		};


	        // create the envelope, add data to it and create the digest
		let envelope = new Envelope(previousHash, mockContent);


		// create wallet to sign the digest
		// TODO: use fixture to generate this wallet
		let w = new Wallet(mnemonic);

		// perform the signature
		envelope.sign(w);


		// recover public key from signature and compare to original public key
		let publicKeyRecoveredBuffer = envelope.recover();
		let publicKeyRecoveredBytes = new Uint8Array(publicKeyRecoveredBuffer);
		assert.deepEqual(publicKeyBytes, publicKeyRecoveredBytes);

	});

	it('json', function() {
		let typ = 'raw';
		let previousHash = zeroHash;
		let mockContent = {
			serialize: function() {
				return Uint8Array.from([0x66, 0x6f, 0x6f]);
			},
			typ: new Uint8Array([42]),
		};
		let ei = new Envelope(previousHash, mockContent);


		let w = new Wallet(mnemonic);


		// perform the signature
		ei.sign(w);


		let j = JSON.stringify(ei);
		let eo = Envelope.fromJSON(j);

		assert.deepEqual(eo, ei);
	});


	it('protobuf', function() {
		let typ = 'raw';
		let previousHash = zeroHash;
		let mockContent = {
			serialize: function() {
				return Uint8Array.from([0x66, 0x6f, 0x6f]);
			},
			typ: new Uint8Array([42]),
		};
		let ei = new Envelope(previousHash, mockContent);

		let w = new Wallet(mnemonic);

		// perform the signature
		ei.sign(w);

		let msgBuf = ei.serialize();

		let eo = Envelope.deserialize(msgBuf);
		assert.deepEqual(eo, ei);

	});
});
