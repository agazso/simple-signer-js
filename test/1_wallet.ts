import { Wallet } from '../wallet';
import * as assert from 'assert';
import { Keccak } from 'sha3';

let mnemonic = "soup high fat equip protect unusual tag helmet detail erupt hover boss glad crew relief narrow message grid swing tone physical inquiry satisfy habit";

describe('wallet', () => {
	it('create', function() {
		let w = new Wallet(mnemonic);
		assert.notEqual(w, undefined);

		let poo = false;
		try {
			w = new Wallet("foo bar baz");
		} catch(e) {
			poo = true;
		}

		assert.ok(poo);
	});


	it('sign buffer data', function() {
		let w = new Wallet(mnemonic);

		let plainText = 'foo';
		let plainTextBuf = Buffer.from(plainText)
		let h = new Keccak(256);
		h.update(plainText);
		let digestBuffer = h.digest();
	});
});
