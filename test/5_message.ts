import * as assert from 'assert';
import * as secp256k1 from 'secp256k1';

import { Message } from '../message';
import { Envelope } from '../envelope';
import { Contact } from '../contact';
import { zeroHash } from '../types';
import { Wallet } from '../wallet';


let mnemonic = "soup high fat equip protect unusual tag helmet detail erupt hover boss glad crew relief narrow message grid swing tone physical inquiry satisfy habit";

describe('message', () => {
	it('create', () => {
		let content = {
			serialize: () => { return new Uint8Array([0x66, 0x6f, 0x6f]); },
			typ: new Uint8Array([42]),
		};
		let envelope = new Envelope(zeroHash, content);
		let contact = new Contact('Melvin Ferd', 'http://minip.no');
		let wallet = new Wallet(mnemonic);
		let publicKey = secp256k1.publicKeyCreate(wallet.key);
		envelope.sign(wallet);
		let message = new Message(envelope, contact);

		let j = JSON.stringify(message);
		let messageRecovered = Message.fromJSON(j);

		assert.deepEqual(message, messageRecovered);

		assert.ok(message.verify());

	});
});
