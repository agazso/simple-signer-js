import * as secp256k1 from 'secp256k1';
import * as bip from 'bip39';
import { Keccak } from 'sha3';
import { Signature } from './interfaces';



class Wallet {
	key: 		Uint8Array
	mnemonic:	string

	constructor(mnemonic: string) {
		if (!bip.validateMnemonic(mnemonic)) {
			throw('invalid mnemonic "' + mnemonic + '"');
		}

		// create the key bytes and compress to 32 bytes
		let privKey = bip.mnemonicToSeedSync(mnemonic);
		let h = new Keccak(256);
		h.update(privKey);
		let privKeyHash = h.digest();

		// check validity of key for secp256k1
		if (!secp256k1.privateKeyVerify(privKeyHash)) {
			throw('invalid private key generated')
		}

		// everything verified, assign to object
		this.key = privKeyHash;
		this.mnemonic = mnemonic;
	}


	public static newMnemonic(randomBytes:Uint8Array): string {
		if (randomBytes.length < 32) {
			throw('not enoung entropy, randomBytes length should be minimum 32')
		}
		let entropy = randomBytes.slice(0, 32);
		let mnemonic = bip.entropyToMnemonic(Buffer.from(entropy));
		return mnemonic;
	}


	public signDigest(digest:Uint8Array): Signature {
		let sigObj = secp256k1.ecdsaSign(digest, this.key);
		return {
		       signature: 	Buffer.from(sigObj.signature),
		       recid:		sigObj.recid,
		       };
	}
}

export { Wallet };
